import { jwtConstants } from './constants';

describe('utils module testing', () => {
  it('should return a JWT secret key', () => {
    expect(jwtConstants).toHaveProperty('JWTsecret');
  });
});
