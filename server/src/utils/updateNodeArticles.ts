import { Injectable, ServiceUnavailableException } from '@nestjs/common';
import { Cron, CronExpression, Timeout } from '@nestjs/schedule';
import * as moment from 'moment';
import { NodeArticleService } from '../nodeArticle/services/nodeArticle.service';
import { request } from 'undici';

@Injectable()
export class GetNodeArticles {
  constructor(private readonly nodeArticleService: NodeArticleService) {}

  @Cron(CronExpression.EVERY_HOUR)
  async updateNodeArticles() {
    try {
      const { body } = await request(
        'https://hn.algolia.com/api/v1/search_by_date?query=nodejs',
      );
      const { hits: articles } = await body.json();
      for (const article of articles) {
        const articleFound = await this.nodeArticleService.findByObjectId(
          article.objectID,
        );

        if (!articleFound) {
          const newArticle = article;
          newArticle.created_at = moment.utc(article.created_at);
          newArticle.object_id = article.objectID;
          newArticle.deleted = false;
          await this.nodeArticleService.create(newArticle);
        }
      }
    } catch (error) {
      throw new ServiceUnavailableException();
    }
  }

  @Timeout(10 * 1000) //Called once, 10 seconds after app starts to initialize the cron object
  onceAfter10Seconds() {
    this.updateNodeArticles();
  }
}
