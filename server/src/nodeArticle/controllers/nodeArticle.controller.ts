import { Controller, Delete, Get, Query, UseGuards } from '@nestjs/common';
import { JwtAuthGuard } from '../../auth/jwt/jwt-auth.guard';
import { NodeArticle } from '../schemas/nodeArticle.schema';
import { NodeArticleService } from '../services/nodeArticle.service';

@Controller('node-articles')
export class NodeArticleController {
  constructor(private readonly nodeArticleService: NodeArticleService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  getNodeArticles(
    @Query('pageNumber') pageNumber: number,
    @Query('author') author: string,
    @Query('_tags') _tags: string[],
    @Query('title') title: string,
    @Query('wordMonth') wordMonth: string,
  ): Promise<object> {
    return this.nodeArticleService.getAll(
      pageNumber,
      author,
      _tags,
      title,
      wordMonth,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Delete()
  updateNodeArticle(
    @Query('object_id') object_id: string,
  ): Promise<NodeArticle> {
    return this.nodeArticleService.updateDeletedStatus(object_id);
  }
}
