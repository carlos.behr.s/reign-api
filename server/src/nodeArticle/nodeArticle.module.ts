import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { NodeArticleSchema } from '../nodeArticle/schemas/nodeArticle.schema';
import { NodeArticleController } from './controllers/nodeArticle.controller';
import { NodeArticleService } from './services/nodeArticle.service';
import { GetNodeArticles } from '../utils/updateNodeArticles';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: 'NodeArticle',
        schema: NodeArticleSchema,
      },
    ]),
  ],
  controllers: [NodeArticleController],
  providers: [GetNodeArticles, NodeArticleService],
})
export class NodeArticleModule {}
