import { Model } from 'mongoose';
import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import {
  NodeArticle,
  NodeArticleDocument,
} from '../schemas/nodeArticle.schema';
import * as moment from 'moment';

@Injectable()
export class NodeArticleService {
  constructor(
    @InjectModel('NodeArticle')
    private nodeArticleModel: Model<NodeArticleDocument>,
  ) {}

  async create(nodeArticle: NodeArticle): Promise<NodeArticle> {
    const newNodeArticle = new this.nodeArticleModel(nodeArticle);
    return newNodeArticle.save();
  }

  async getAll(
    pageNumber: number,
    author: string,
    _tags: string[],
    title: string,
    wordMonth: string,
  ): Promise<object> {
    const pageLimit = 5;
    const filterConfig = []; //array containing the filtering criteria
    let articlesCount = 0;

    if (author) {
      const configQuery = {
        author: { $regex: author ? author : '', $options: 'i' },
      };
      filterConfig.push(configQuery);
    }

    if (title) {
      const configQuery = [
        { title: { $regex: title ? title : '', $options: 'i' } },
        { story_title: { $regex: title ? title : '', $options: 'i' } },
      ];
      filterConfig.push(...configQuery);
    }

    if (_tags) {
      const configQuery = { _tags: { $all: _tags } };
      filterConfig.push(configQuery);
    }

    if (wordMonth) {
      const configQuery = {
        created_at: {
          $gte: moment(wordMonth, 'MMMM').startOf('month'),
          $lte: moment(wordMonth, 'MMMM').endOf('month'),
        },
      };
      filterConfig.push(configQuery);
    }

    //if no params have been sent in the request
    if (filterConfig.length === 0) {
      filterConfig.push({ author: { $regex: '', $options: 'i' } });
    }
    const articlesRetrieved = await this.nodeArticleModel
      .find({
        $or: filterConfig,
        deleted: false,
      })
      .limit(pageLimit)
      .sort({ created_at: -1 })
      .skip(
        Number(pageNumber) === 0 || Number(pageNumber) === 1 //pagination will start at page 1
          ? 0
          : pageNumber * pageLimit - pageLimit,
      )
      .exec();
    if (articlesRetrieved.length > 0) {
      articlesCount = await this.nodeArticleModel.count();
    }
    return {
      articles: articlesRetrieved,
      maxPages: Math.ceil(Number(articlesCount) / pageLimit), //tech debt - to be improved
      actualPage: pageNumber,
    };
  }

  async findByObjectId(object_id: string): Promise<NodeArticle> {
    return this.nodeArticleModel.findOne({ object_id }).exec();
  }

  async updateDeletedStatus(object_id: string): Promise<NodeArticle> {
    const filter = { object_id };
    const update = { deleted: true };
    return this.nodeArticleModel.findOneAndUpdate(filter, update).exec();
  }
}
