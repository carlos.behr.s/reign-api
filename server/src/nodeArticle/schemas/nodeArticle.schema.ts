import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type NodeArticleDocument = NodeArticle & Document;

@Schema({ collection: 'node_articles' })
export class NodeArticle {
  @Prop()
  author: string;

  @Prop([String])
  _tags: string[];

  @Prop()
  title: string;

  @Prop()
  story_title: string;

  @Prop()
  story_text: string;

  @Prop()
  comment_text: string;

  @Prop()
  object_id: string;

  @Prop()
  created_at: Date;

  @Prop()
  deleted: boolean; //used to prevent displaying this article if it has been "deleted"
}

export const NodeArticleSchema = SchemaFactory.createForClass(NodeArticle);
