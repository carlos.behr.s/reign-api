import { Injectable } from '@nestjs/common';

export type User = any;

@Injectable() //tech debt - should be implemented using a database
export class UsersService {
  private readonly users = [
    {
      username: 'Carlos',
      password: 'reign',
    },
  ];

  async findOne(username: string): Promise<User | undefined> {
    return this.users.find((user) => user.username === username);
  }
}
