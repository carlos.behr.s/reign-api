import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';

@Injectable()
export class AuthService {
  constructor(private jwtTokenService: JwtService) {}

  //tech debt - validation of request can be implemented
  async loginWithCredentials(user: any) {
    const payload = { username: user.username, password: user.password };
    return {
      access_token: this.jwtTokenService.sign(payload),
    };
  }
}
