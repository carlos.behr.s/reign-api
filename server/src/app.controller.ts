import { Controller, Post, Request } from '@nestjs/common';
import { AuthService } from './auth/services/auth.service';

@Controller()
export class AppController {
  constructor(private authService: AuthService) {}

  @Post('login')
  async login(@Request() req) {
    return this.authService.loginWithCredentials(req.body);
  }
}
