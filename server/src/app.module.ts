import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { ScheduleModule } from '@nestjs/schedule';
import { AppController } from './app.controller';
import { NodeArticleModule } from './nodeArticle/nodeArticle.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
    AuthModule,
    ScheduleModule.forRoot(),
    NodeArticleModule,
    MongooseModule.forRoot('mongodb://mongodb/reign-database'),
  ],
  controllers: [AppController],
})
export class AppModule {}
