# Reign Api

Demo for Reign!

## A little bit of context

This API will return Node articles posted in Hacker News.
In order to be able to navigate through the endpoints you have to provide a bearer token (JWT) in the headers.
To get an authorization token you will have to login first.

## Getting started

VERY IMPORTANT!! You have to have [Docker](https://docs.docker.com/desktop/windows/install/) installed and it's Engine running on your PC.

Note: The installation of Docker Deskstop also installs Docker Compose (needed later to run the Docker containers).

## Get the code!

Follow these sentences in order to get the code from the repo

```
cd <your preferred location>
git clone https://gitlab.com/carlos.behr.s/reign-api.git
cd reign-api
```

## Initialize the server

Run the following command on the same folder as the previous step

```
docker-compose up
```

## Postman & login

You can find a postman collection with queries ready to use attached to the root of the [project's repo](https://gitlab.com/carlos.behr.s/reign-api)

## Mongo Express & Swagger!!

- Open [http://localhost:3001](http://localhost:3001) to check the instance of Mongo-Express.
- Open [http://localhost:3000/api/docs](http://localhost:3000/api/docs) to open Swagger.
